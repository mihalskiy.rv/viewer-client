/**
 * @module Sagas/GitHub
 * @desc GitHub
 */

import { now, request } from '@gilbarbara/helpers';
import { all, call, put, select, takeLatest } from 'redux-saga/effects';

import { hasValidCache } from 'modules/helpers';

import { ActionTypes } from 'literals';

import { StoreAction } from 'types';

/**
 * Get Repos
 *
 * @param {Object} action
 *
 */
export function* getRepos({ payload }: StoreAction) {
  const { query } = payload;

  try {
    const { updatedAt = 0 } = yield select(s => s.github.topics || {});
    const hasCache = hasValidCache(updatedAt);
    let items;

    if (!hasCache) {
      ({ items = [] } = yield call(
        request,
        `https://mocki.io/v1/27dbdfde-9de3-4677-81a1-d64fe0f3905f`,
      ));
    }

    if (query && items) {
      items = items.filter((item: { category: any }) => item.category === query);
    }

    yield put({
      type: ActionTypes.GITHUB_GET_REPOS_SUCCESS,
      payload: items,
      meta: { cached: hasCache, query, updatedAt: now() },
    });
  } catch (err) {
    yield put({
      type: ActionTypes.GITHUB_GET_REPOS_FAILURE,
      payload: err,
      meta: { query },
    });
  }
}

/**
 * GitHub Sagas
 */
export default function* root() {
  yield all([takeLatest(ActionTypes.GITHUB_GET_REPOS_REQUEST, getRepos)]);
}
