import React from 'react';
import { Box } from 'styled-minimal';

import Github from 'containers/GitHub';

function NewsList() {
  return (
    <Box key="Private" data-testid="Private">
      <Github />
    </Box>
  );
}

export default NewsList;
