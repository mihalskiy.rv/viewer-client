import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import styled from 'styled-components';
import { useMount, usePrevious, useUpdateEffect } from 'react-use';
import {
  Button,
  Flex
} from 'styled-minimal';

import { useShallowEqualSelector } from 'modules/hooks';
import { StoreState, UserState } from '../types';
import { themeColor } from '../modules/theme';
import { STATUS } from 'literals';

import { getRepos, showAlert } from 'actions';

import Icon from './Icon';

const Wrapper = styled.div`
  position: absolute;
  left: 160px;
  display: flex;
  @media(max-width: 800px) {
    left: 20px;
    top: 70px;
    margin-bottom: 20px;
  }
`;

const InputFilter = styled.input`
  width: 70%;
  border: none;
  border-bottom: 1px solid #C3C4C8;
  background: #F4F5FA;
  color: #C3C4C8;
  margin-left: 25px;
  font-size: 18px;
  &:focus {
    outline: none;
  }
  @media (min-width: 550px) {
    display: none;
  }
  @media (max-width: 550px) {
    width: 70%;
    margin: 0 5px 0 0;
  }
`

const ButtonFilter = styled(Button)<Pick<UserState, 'isTheme'>>`
  border: none;
  background: transparent;
  color: ${({ isTheme }) => (isTheme === 'light' ? themeColor.listColor.light.color : themeColor.listColor.dark.color)};
  font-family: Montserrat;
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  line-height: 22px;
  padding: 12px 0;
  margin-right: 49px;
  width: auto;
  img {
    width: 22px;
  }
  svg {
    fill: ${({ isTheme }) => (isTheme === 'light' ? '#000' : '#fff')};
  }

  &:hover {
    cursor: pointer;
  }
  @media(max-width: 550px) {
    margin-right: 28px;
    font-size: 24px;
    font-weight: bold;
    &:last-child {
      width: 50px;
    }
  }
  @media(max-width: 480px) {
    width: auto;
    display: flex;
    justify-content: start;
  }

  &:last-child {
    margin: 0;
  }
`;

export default function ListFilter() {
    const dispatch = useDispatch();
    const { isTheme } = useShallowEqualSelector((s: StoreState) => s.app);
    const { message, query, status } = useShallowEqualSelector(({ github }) => ({
        message: github.topics[github.query]?.message || '',
        query: github.query,
        status: github.topics[github.query]?.status || STATUS.IDLE,
    }));
    const previousStatus = usePrevious(status);

    useMount(() => {
        dispatch(getRepos(query || ''));
      });
    
    useUpdateEffect(() => {
    if (previousStatus !== status && status === STATUS.ERROR) {
        dispatch(showAlert(message, { variant: 'danger' }));
    }
    }, [dispatch, message, previousStatus, status]);

    const handleClick = useCallback(
        (e: React.MouseEvent<HTMLButtonElement>) => {
          const { topic = '' } = e.currentTarget.dataset;
    
          dispatch(getRepos(topic));
        },
        [dispatch],
      );
    
    const isRunning = status === STATUS.RUNNING;

    return (
      <Wrapper>
          <Flex justifyContent="start" flexWrap="wrap">
            <ButtonFilter isTheme={isTheme}
              busy={query === 'popular' && isRunning}
              data-topic="popular"
              invert={query !== 'popular'}
              onClick={handleClick}
              size="lg"
            >
              Popular
            </ButtonFilter>
            <ButtonFilter isTheme={isTheme}
              busy={query === 'upVoted' && isRunning}
              data-topic="upVoted"
              invert={query !== 'upVoted'}
              onClick={handleClick}
              size="lg"
            >
              Upvoted
            </ButtonFilter>
            <ButtonFilter isTheme={isTheme}
              busy={query === 'discussed' && isRunning}
              data-topic="discussed"
              invert={query !== 'discussed'}
              onClick={handleClick}
              size="lg"
            >
              Discussed
            </ButtonFilter>
            <ButtonFilter isTheme={isTheme}
              busy={query === 'recent' && isRunning}
              data-topic="recent"
              invert={query !== 'recent'}
              onClick={handleClick}
              size="lg"
            >
              Recent
            </ButtonFilter>
            <InputFilter placeholder="Search" />
            <ButtonFilter isTheme={isTheme}
              size="lg"
            >
              <Icon name="search" width={22}/>
            </ButtonFilter>
        </Flex>
      </Wrapper>
    )
}