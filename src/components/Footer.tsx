import React from 'react';
import { Box, Container, Flex } from 'styled-minimal';

import { useShallowEqualSelector } from '../modules/hooks';
import { themeColor } from '../modules/theme';
import { StoreState } from '../types';

function Footer() {
  const { isTheme } = useShallowEqualSelector((s: StoreState) => s.app);
  return (
    <Box background={isTheme === 'light' ? themeColor.mainColor.light.background : themeColor.mainColor.dark.background} as="footer" borderTop="0.1rem solid #ddd">
      <Container py={3}>
        <Flex justifyContent="space-between">
          <h1>Footer</h1>
        </Flex>
      </Container>
    </Box>
  );
}

export default Footer;
