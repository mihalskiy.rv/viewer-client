import React from 'react';
import styled from 'styled-components';
import { Container } from 'styled-minimal';
import { useDispatch } from 'react-redux';

import { spacer } from 'modules/theme';

import Logo from 'components/Logo';
import Slider from 'components/Slider';
import ListFilter from './ListFilter';
import Icon from './Icon';

import { openSettings } from '../actions/app';

import { useShallowEqualSelector } from '../modules/hooks';
import { StoreState, UserState } from '../types';
import { themeColor } from '../modules/theme';

const HeaderWrapper = styled.header<Pick<UserState, 'isTheme'>>`
  padding: 0 75px;
  ${({ isTheme }) => (isTheme === 'light' ? themeColor.mainColor.light : themeColor.mainColor.dark)};
  left: 0;
  position: fixed;
  right: 0;
  top: 0;
  z-index: 200;

  @media(max-width: 800px) {
    height: 150px;
  }

  @media (max-width: 1920px) {
    padding: 20px 43px;
  }

  @media (min-width: 1920px) {
    padding: 20px 43px;
  }

  @media(max-width: 551px) {
    height: 240px;
  }


  &:before {
    ${({ isTheme }) => (isTheme === 'light' ? themeColor.line.light : themeColor.line.dark)};
    bottom: 0;
    content: '';
    height: 0.2rem;
    left: 0;
    position: absolute;
    right: 0;

    @media(max-width: 551px) {
      display: none;
    }
  }
`;

const HeaderContainer = styled(Container)`
  align-items: center;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  padding-bottom: ${spacer(2)};
  padding-top: ${spacer(2)};
  @media(max-width: 551px) {
    padding: 10px 25px;
  }
`;

const LogoWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  width: 660px;
  @media (max-width: 750px) {
    width: 35%;
  }
`;

const SettingsWrapper = styled.div<Pick<UserState, 'isTheme'>>`
  display: flex;
  align-items: center;
  justify-content: center;

  svg {
    fill: ${({ isTheme }) => (isTheme === 'light' ? '#000' : '#92B3E0')};
  }
`;

const IconWrap = styled.div<Pick<UserState, 'isTheme'>>`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  border: 1px solid ${({ isTheme }) => (isTheme === 'light' ? '#A5B5CA' : '#2F3D60')};
  text-align: center;
  &:hover {
    cursor: pointer;
  }
`;

const Theme = styled.div`
  @media (max-width: 1450px) {
    display: none;
  }
`

const TextWrap = styled.div<Pick<UserState, 'isTheme' | 'fontSize'>>`
  font-family: Montserrat;
  font-style: normal;
  color: ${({ isTheme }) => (isTheme === 'light' ? '#000' : '#92B3E0')};
  font-size: ${props => `${props.fontSize}px` || '12px'};
  font-weight: 500;
  line-height: 16px;

  &:hover {
    cursor: pointer;
  }
`;

const WrapTitle = styled.div`
  width: 230px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-right: 60px;
  @media (max-width: 1450px) {
    margin-right: 154px;
  }
  @media (max-width: 1290px) {
    display: none
  }
`

export default function Header() {

  const settings = true;

  const dispatch = useDispatch();
  const { isTheme } = useShallowEqualSelector((s: StoreState) => s.app);

  const settingsOpen = () => {
    dispatch(openSettings(settings));
  }

  return (
    <HeaderWrapper data-testid="Header" isTheme={isTheme}>
      <HeaderContainer>
        <LogoWrapper>
          <Logo />
          <ListFilter />
        </LogoWrapper>
        <SettingsWrapper isTheme={isTheme}>
          <WrapTitle>
            <TextWrap fontSize={14} isTheme={isTheme}>Terms of service</TextWrap>
            <TextWrap fontSize={14} isTheme={isTheme}>Privacy policy</TextWrap>
          </WrapTitle>
          <Theme>
            <TextWrap fontSize={12} isTheme={isTheme}>{isTheme === 'light' ? 'Dark theme' : 'Light theme'}</TextWrap>
          </Theme>
          <Slider type="changeTheme"/>
          <div onClick={settingsOpen}>
            <IconWrap isTheme={isTheme} >
              <Icon name="settings" width={18}/>
            </IconWrap>
          </div>
        </SettingsWrapper>
      </HeaderContainer>
    </HeaderWrapper>
  );
}
