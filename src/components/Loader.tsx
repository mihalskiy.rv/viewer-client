import React from 'react';
import styled, { keyframes } from 'styled-components';

/* stylelint-disable unit-disallowed-list */
const Dash = keyframes`
  0%,
  100% {
    box-shadow: 0 -13px 0 0 transparent, 9px -9px 0 0 transparent, 13px 0 0 0 #fff, 9px 9px 0 0 #fff, 0 13px 0 0 #fff, -9px 9px 0 0 #fff, -13px 0 0 0 #fff, -9px -9px 0 0 #fff;
  }
  12.5% {
    box-shadow: 0 -13px 0 0 #fff, 9px -9px 0 0 transparent, 13px 0 0 0 transparent, 9px 9px 0 0 #fff, 0 13px 0 0 #fff, -9px 9px 0 0 #fff, -13px 0 0 0 #fff, -9px -9px 0 0 #fff;
  }
  25% {
    box-shadow: 0 -13px 0 0 #fff, 9px -9px 0 0 #fff, 13px 0 0 0 transparent, 9px 9px 0 0 transparent, 0 13px 0 0 #fff, -9px 9px 0 0 #fff, -13px 0 0 0 #fff, -9px -9px 0 0 #fff;
  }
  37.5% {
    box-shadow: 0 -13px 0 0 #fff, 9px -9px 0 0 #fff, 13px 0 0 0 #fff, 9px 9px 0 0 transparent, 0 13px 0 0 transparent, -9px 9px 0 0 #fff, -13px 0 0 0 #fff, -9px -9px 0 0 #fff;
  }
  50% {
    box-shadow: 0 -13px 0 0 #fff, 9px -9px 0 0 #fff, 13px 0 0 0 #fff, 9px 9px 0 0 #fff, 0 13px 0 0 transparent, -9px 9px 0 0 transparent, -13px 0 0 0 #fff, -9px -9px 0 0 #fff;
  }
  62.5% {
    box-shadow: 0 -13px 0 0 #fff, 9px -9px 0 0 #fff, 13px 0 0 0 #fff, 9px 9px 0 0 #fff, 0 13px 0 0 #fff, -9px 9px 0 0 transparent, -13px 0 0 0 transparent, -9px -9px 0 0 #fff;
  }
  75% {
    box-shadow: 0 -13px 0 0 #fff, 9px -9px 0 0 #fff, 13px 0 0 0 #fff, 9px 9px 0 0 #fff, 0 13px 0 0 #fff, -9px 9px 0 0 #fff, -13px 0 0 0 transparent, -9px -9px 0 0 transparent;
  }
  87.5% {
    box-shadow: 0 -13px 0 0 transparent, 9px -9px 0 0 #fff, 13px 0 0 0 #fff, 9px 9px 0 0 #fff, 0 13px 0 0 #fff, -9px 9px 0 0 #fff, -13px 0 0 0 #fff, -9px -9px 0 0 transparent;
  }
  }
`;

const LoaderWrapper = styled.div`
  margin: 0 auto;
  margin-top: 60px;
  padding: 0 10px 0 10px;
  width: 154px;
  height: 50px;
  background: #008FFF;
  border-radius: 100px;
  display: flex;
  justify-content: space-around;
  align-items: center;
  color: #fff;

  p {
    font-family: Montserrat;
    font-weight: 600;
    font-size: 18px;
    line-height: 22px;
  }
`

const Loading = styled.div`
  width: 5px;
  height: 5px;
  border-radius: 50%;
  text-indent: -9999em;
  -webkit-animation: ${Dash} 1s infinite ease;
  animation: ${Dash} 1s infinite ease;
  -webkit-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);
`


const Loader = () => {
  return (
    <LoaderWrapper>
      <Loading />
      <p>loading</p>
    </LoaderWrapper>
  );
};

export default Loader;
