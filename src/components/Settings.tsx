import React from 'react';
import { useDispatch } from 'react-redux';
import styled, { keyframes } from 'styled-components';

import { SliderSettings } from './Slider';

import { openSettings } from '../actions/app';

const SettingOpen = keyframes`
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
`;

const Wrapper = styled.div`
  width: 805px;
  height: 430px;
  background-color: #fff;
  padding: 57px 60px;
  font-family: Montserrat;
  animation: ${SettingOpen} 0.7s 1;
`;

const SettingsHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 30px;
  font-family: Montserrat;
  font-style: normal;
  font-weight: bold;
  font-size: 22px;
  line-height: 27px;
  margin-bottom: 48px;
  h4 {
    margin: 0;
  }
`;

const Label = styled.label`
  cursor: pointer;
  display: flex;
  align-items: center;
  p {
    margin: 0;
  }
`;

const WrapperBody = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: start;
  margin-bottom: 60px;
  p {
    font-family: Montserrat;
    font-style: normal;
    font-weight: 600;
    font-size: 18px;
    line-height: 22px;
    padding-left: 8px;
    margin: 0;
    margin-bottom: 16px;
  }

  span {
    font-weight: 500;
    font-size: 14px;
    line-height: 17px;
  }

  div.first {
    width: 21%;
    p {
      padding: 0;
    }
  }

  div.second {
    width: 23%;
  }

  div.third {
    width: 22%;
  }

  div.fourth {
    width: 34%;
    p {
      padding-left: 18px;
    }
    label {
      margin-bottom: 16px;
    }
  }
`;

const Input = styled.input`
  margin: 0;
  margin-right: 20px;
  margin-bottom: 16px;
  cursor: pointer;
  position: relative;
  height: 18px;
  width: 18px;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  outline: none;
  &::before {
    content: '';
    position: absolute;
    top: 50%;
    left: 50%;
    width: 18px;
    height: 18px;
    margin: 8px;
    border-radius: 50%;
    transform: translate(-50%, -50%);
    background-color: white;
    border: 2px solid #008fff;
  }
  &:checked {
    &::after {
      content: '';
      position: absolute;
      top: 50%;
      left: 50%;
      width: 8px;
      height: 8px;
      margin: 8px;
      border-radius: 50%;
      background-color: #000;
      transform: translate(-50%, -50%);
      visibility: visible;
    }
  }
`;

const Close = styled.div`
  &:hover {
    cursor: pointer;
  }
`;

const Button = styled.div`
  width: 154px;
  height: 50px;
  background: #008fff;
  border-radius: 100px;
  font-weight: 600;
  font-size: 18px;
  line-height: 22px;
  color: #ffffff;
  padding: 14px 22px;
  &:hover {
    cursor: pointer;
    background: #fff;
    border: 2px solid #008fff;
    color: #008fff;
  }
`;

export default function Settings() {
  const dispatch = useDispatch();

  const settingsOpen = (settings: boolean) => {
    dispatch(openSettings(settings));
  };

  return (
    <Wrapper>
      <SettingsHeader>
        <h4>Settings</h4>
        <Close onClick={() => settingsOpen(false)}>&#x2715;</Close>
      </SettingsHeader>
      <WrapperBody>
        <div className="first">
          <p>Layout</p>
          <p>Flow1</p>
          <p>Flow2</p>
        </div>
        <div className="second">
          <p>Theme</p>
          <Label>
            <Input name="theme" type="radio" />
            <span>Dark</span>
          </Label>
          <Label>
            <Input name="theme" type="radio" />
            <span>Light</span>
          </Label>
          <Label>
            <Input name="theme" type="radio" />
            <span>Auto</span>
          </Label>
        </div>
        <div className="third">
          <p>Density</p>
          <Label>
            <Input name="density" type="radio" />
            <span>Eco</span>
          </Label>
          <Label>
            <Input name="density" type="radio" />
            <span>Rommy</span>
          </Label>
          <Label>
            <Input name="density" type="radio" />
            <span>Cozy</span>
          </Label>
        </div>
        <div className="fourth">
          <p>Preferences</p>
          <Label>
            <SliderSettings />
            <span>Hide read posts</span>
          </Label>
          <Label>
            <SliderSettings />
            <span>Open links in new tab</span>
          </Label>
        </div>
      </WrapperBody>
      <Button onClick={() => settingsOpen(false)}>Ok</Button>
    </Wrapper>
  );
}
