import React from 'react';
import styled from 'styled-components';

import { useShallowEqualSelector } from 'modules/hooks';
import { StoreState, UserState } from '../types';

export const Wrapper = styled.div`
  align-items: flex-start;
  display: inline-flex;
  font-size: 0;

  svg {
    height: 4.2rem;
    max-height: 100%;
    width: auto;
  }
`;

const LogoText = styled.div<Pick<UserState, 'isTheme'>>`
  width: 70px;
  height: 40px;
  font-family: Montserrat;
  font-style: normal;
  font-weight: bold;
  font-size: 24px;
  line-height: 20px;
  text-transform: uppercase;
  color: ${({ isTheme }) => (isTheme === 'light' ? '#000' : '#fff')}
`

function Logo() {
  const { isTheme } = useShallowEqualSelector((s: StoreState) => s.app);
  return (
    <Wrapper>
      <LogoText isTheme={isTheme}>
        Next Page
      </LogoText>
    </Wrapper>
  );
}

export default Logo;
