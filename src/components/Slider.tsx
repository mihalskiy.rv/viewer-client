import React, { useState } from 'react';
import styled from 'styled-components';

import { useShallowEqualSelector } from '../modules/hooks';
import { StoreState, UserState } from '../types';
import { themeColor } from '../modules/theme';

import { useDispatch } from 'react-redux';
import { changeTheme } from '../actions';

const Wrapper = styled.div`
    display: flex;
    margin-right: 44px;
    margin-left: 0;

    &:hover {
        cursor: pointer;
    }

    @media(max-width: 900px) {
        display: none;
    }
`;

const SliderWrapper = styled.div<Pick<UserState, 'isTheme'>>`
    ${({ isTheme }) => (isTheme === 'light' ? themeColor.sliderWrapper.light : themeColor.sliderWrapper.dark)}
    width: 36px;
    height: 20px;
    position: relative;
    top: 2px;
    border-radius: 25px;
`;

const SliderInner = styled.div<Pick<UserState, 'isTheme'>>`
    ${({ isTheme }) => (isTheme === 'light' ? themeColor.sliderInner.light : themeColor.sliderInner.dark)}
    width: 24px;
    height: 24px;
    background: #FFFFFD;
    position: relative;
    box-shadow: 0px 0px 6px rgba(0, 0, 0, 0.3);
    border-radius: 50%;
    z-index: 1;
    transition: 0.3s all;
`;

const WrapperSettings = styled.div`
    display: flex;
    margin-right: 10px;

    &:hover {
        cursor: pointer;
    }
`;

const SliderSettingsWrapper = styled.div`
    background: #C8CAD7;
    border: 1px solid #A5B5CA;
    width: 36px;
    height: 20px;
    position: relative;
    top: 2px;
    border-radius: 25px;
`;

const SliderSettingsInner = styled.div`
    right: -20px;
    width: 24px;
    height: 24px;
    background: #FFFFFD;
    position: relative;
    box-shadow: 0px 0px 6px rgba(0, 0, 0, 0.3);
    border-radius: 50%;
    z-index: 1;
    transition: 0.3s all;
`;

export default function Slider({ type } : { type:string }) {

    const [colors, setColors] = useState(true);
    const dispatch = useDispatch();
    
    const change = () => {
        if (type === 'changeTheme') {
            setColors(!colors);
            dispatch(changeTheme(colors));
        }
    }
    const { isTheme } = useShallowEqualSelector((s: StoreState) => s.app);

    return (
        <Wrapper onClick={change}>
            <SliderInner isTheme={isTheme} />
            <SliderWrapper isTheme={isTheme} />
        </Wrapper>
    )
};

export function SliderSettings() {
    return (
        <WrapperSettings>
            <SliderSettingsInner />
            <SliderSettingsWrapper />
        </WrapperSettings>
    )
};