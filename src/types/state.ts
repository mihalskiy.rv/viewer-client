import { Dispatch } from 'redux';
import { Variants } from 'styled-minimal/lib/types';
import { ValueOf } from 'type-fest';

import { AlertPosition, Icons, Status } from './common';

export interface AlertData {
  icon: Icons;
  id: string;
  message: string;
  position: AlertPosition;
  timeout: number;
  variant: Variants;
}

export interface Topic {
  cached: boolean;
  data: [];
  message: string;
  status: ValueOf<Status>;
  updatedAt: number;
}

export interface AppState {
  alerts: AlertData[];
  isTheme: string;
  isSettingOpen: boolean;
}

export interface GitHubState {
  query: string;
  topics: Record<string, Topic>;
}

export interface UserState {
  isAuthenticated: boolean;
  isTheme: string;
  status: ValueOf<Status>;
  fontSize: number;
}

export interface StoreState {
  app: AppState;
  github: GitHubState;
  user: UserState;
}

export interface WithDispatch {
  dispatch: Dispatch;
}
