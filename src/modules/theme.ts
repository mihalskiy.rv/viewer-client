import is from 'is-lite';
import { getTheme, px } from 'styled-minimal';

export const headerHeight = 98;

export const appColor = '#00b4d5';

export const easing = 'cubic-bezier(0.35, 0.01, 0.77, 0.34);';

const theme = getTheme({
  button: {
    borderRadius: {
      xs: 4,
      lg: 28,
      xl: 32,
    },
    padding: {
      lg: [12, 28],
      xl: [14, 32],
    },
  }
});

export const themeColor = {
  sliderWrapper: {
    light: {
      background: '#C8CAD7',
      border: '1px solid #A5B5CA'
    },
    dark: {
      background: '#021539',
      border: '1px solid #2F3D60'
    }
  },
  sliderInner: {
    light: {
      right: '-20px'
    },
    dark: {
      right: '-41px',
    }
  },
  mainColor: {
    light: {
      background: '#F4F5FA',
    },
    dark: {
      background: '#192744',
    }
  },
  listColor: {
    light: {
      background: '#FFFFFF',
      border: '1px solid #FFFFFF',
      color: '#000',
      boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.1)'
    },
    dark: {
      background: '#2F3D60',
      border: 'none',
      color: '#C6DEFF',
      boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.1)'
    }
  },
  line: {
    light: {
      background: '#C8CAD7'
    },
    dark: {
      background: '#2F3D60'
    }
  }
}

export const variants = theme.colors;
export const spacer = (value: number | string): string =>
  px(is.string(value) ? value : theme.space[value]);

export default theme;
