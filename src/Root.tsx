import React from 'react';
import { Helmet } from 'react-helmet-async';
import { Route, Router, Switch } from 'react-router-dom';
import styled, { ThemeProvider } from 'styled-components';

import history from 'modules/history';
import { useShallowEqualSelector } from 'modules/hooks';
import theme, { headerHeight } from 'modules/theme';

import config from 'config';

import Header from 'components/Header';
import RoutePublic from 'containers/RoutePublic';
import SystemAlerts from 'containers/SystemAlerts';
import NewsList from 'routes/NewsList';
import NotFound from 'routes/NotFound';

import { StoreState, UserState } from 'types';

import Settings from './components/Settings';

const AppWrapper = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  opacity: 1 !important;
  position: relative;
  transition: opacity 0.5s;
`;

const WrapperSettings = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background: rgba(0, 0, 0, 0.5);
`;

const Main = styled.main<Pick<UserState, 'isAuthenticated' | 'isTheme'>>`
  min-height: 100vh;
  padding: 0;
  padding-top: ${({ isAuthenticated }) => (isAuthenticated ? `${headerHeight - 5}px` : 0)};
  background-color: ${({ isTheme }) => (isTheme === 'light' ? '#F4F5FA' : '#192744')};

  @media (max-width: 800px) {
    padding-top: 150px;
  }

  @media (max-width: 551px) {
    padding-top: 240px;
  }
`;

function Root() {
  const { isAuthenticated } = useShallowEqualSelector((s: StoreState) => s.user);
  const { isTheme } = useShallowEqualSelector((s: StoreState) => s.app);
  const { isSettingOpen } = useShallowEqualSelector((s: StoreState) => s.app);

  return (
    <Router history={history}>
      <ThemeProvider theme={theme}>
        <AppWrapper data-testid="app">
          <Helmet
            defaultTitle={config.name}
            defer={false}
            encodeSpecialCharacters
            htmlAttributes={{ lang: 'pt-br' }}
            titleAttributes={{ itemprop: 'name', lang: 'pt-br' }}
            titleTemplate={`%s | ${config.name}`}
          >
            <link
              href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,700;1,400;1,700&display=swap"
              rel="stylesheet"
            />
          </Helmet>
          {isAuthenticated && <Header />}
          <Main isAuthenticated={isAuthenticated} isTheme={isTheme}>
            <Switch>
              <RoutePublic component={NewsList} isAuthenticated={isAuthenticated} path="/" />
              <Route component={NotFound} />
            </Switch>
          </Main>
          {isSettingOpen && (
            <WrapperSettings>
              <Settings />
            </WrapperSettings>
          )}
          <SystemAlerts />
        </AppWrapper>
      </ThemeProvider>
    </Router>
  );
}

export default Root;
