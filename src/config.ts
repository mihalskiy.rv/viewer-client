/**
 * Configuration
 * @module config
 */

const config = {
  name: 'Next Page',
  description: 'Next Page news',
};

export default config;
