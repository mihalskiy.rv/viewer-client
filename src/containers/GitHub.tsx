import React from 'react';
import { useDispatch } from 'react-redux';
import { useMount, usePrevious, useUpdateEffect } from 'react-use';
import styled from 'styled-components';
import {
  Button,
  Image,
  Link
} from 'styled-minimal';

import { useShallowEqualSelector } from 'modules/hooks';
import { themeColor } from 'modules/theme';
import { StoreState, UserState } from '../types';


import { STATUS } from 'literals';

import { getRepos, showAlert } from 'actions';

import Loader from 'components/Loader';

const MainWrapper = styled.div`
  margin: 0 auto;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  margin-top: 30px;
  @media (max-width: 1200px) {
    justify-content: center;
  }

  @media (min-width: 1499px) {
    padding: 0 205px;
  }

  @media (min-width: 2278px) {
    padding: 0 510px;
  }
`

const Item = styled(Link)<Pick<UserState, 'isTheme'>>`
  position: relative;
  width: 335px;
  height: 375px;
  ${({ isTheme }) => (isTheme === 'light' ? themeColor.listColor.light : themeColor.listColor.dark)};
  box-shadow: ${({ isTheme }) => (isTheme === 'light' ? themeColor.listColor.light.boxShadow : themeColor.listColor.dark.boxShadow)};
  align-items: center;
  padding: 0;
  margin: 0 12px 24px 12px;
  display: flex;
  flex-direction: column;
  overflow: hidden;
  justify-content: space-between;
  p {
    color: ${({ isTheme }) => (isTheme === 'light' ? themeColor.listColor.light.color : themeColor.listColor.dark.color)};
  }

  img {
    min-height: 201px;
    margin-bottom: 20px;
  }
  &:hover {
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.1);
  }
`;

const ReadButton = styled(Button)`
  display: none;
  width: 170px;
  height: 50px;
  border-radius: 100px;
  background: transparent;
  border: 1px solid #FFFFFF;
  justify-content: center;
  position: absolute;
  bottom: 86px;
  font-weight: 600;
  font-size: 16px;
  line-height: 20px;
  letter-spacing: 0.5px;
  font-family: Montserrat;
  ${Item}:hover & {
    display: block;
  }
`;

const Eclipse = styled.div`
  display: none;
  position: absolute;
  width: 100%;
  min-height: 201px;
  bottom: 0;
  background: linear-gradient(0deg, rgba(0, 143, 255, 0.6), rgba(0, 143, 255, 0.6));
  ${Item}:hover & {
    display: block;
  }
`

const ItemHeader = styled.div<Pick<UserState, 'isTheme'>>`
  font-family: Montserrat;
  width: 100%;
  font-weight: 600;
  font-size: 22px;
  line-height: 27px;
  padding: 20px 15px 15px 20px;
  color: ${({ isTheme }) => (isTheme === 'light' ? themeColor.listColor.light.color : themeColor.listColor.dark.color)};
`;

const WrapperText = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  align-items: flex-start;
  padding: 0 15px 15px 20px;
  font-family: Museo Sans Cyrl;
  font-weight: 300;
  font-size: 16px;
  line-height: 19px;
`

function GitHub() {
  const dispatch = useDispatch();
  const { isTheme } = useShallowEqualSelector((s: StoreState) => s.app);
  const { data, message, query, status } = useShallowEqualSelector(({ github }) => ({
    data: github.topics[github.query]?.data || [],
    message: github.topics[github.query]?.message || '',
    query: github.query,
    status: github.topics[github.query]?.status || STATUS.IDLE,
  }));
  const previousStatus = usePrevious(status);

  useMount(() => {
    dispatch(getRepos(query || ''));
  });

  useUpdateEffect(() => {
    if (previousStatus !== status && status === STATUS.ERROR) {
      dispatch(showAlert(message, { variant: 'danger' }));
    }
  }, [dispatch, message, previousStatus, status]);

  let output;

  if (status === STATUS.SUCCESS) {
    if (data.length) {
      output = (
        <MainWrapper>
          {data.map((d: Record<string, any>) => (
            <Item key={d.author} href={d.url} target="_blank" isTheme={isTheme}>
              <Eclipse />
              <ReadButton>Read More &#8594;</ReadButton>
              <ItemHeader isTheme={isTheme}>{d.author}</ItemHeader>
              <WrapperText>{d.title}</WrapperText>
              <Image alt={d.author} src={d.img} style={{
                marginBottom: 0
              }} />
            </Item>
          ))}
        </MainWrapper>
      );
    } else {
      output = <h3>Nothing found</h3>;
    }
  } else {
    output = <Loader />;
  }

  return (
    <div key="GitHub" data-testid="GitHubWrapper">
      {output}
    </div>
  );
}

export default GitHub;
