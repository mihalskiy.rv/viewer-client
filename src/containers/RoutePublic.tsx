import React from 'react';
import { Route } from 'react-router-dom';

import { RouteProps } from 'types';

function RoutePublic({ component: Component, isAuthenticated, to = '/', ...rest }: RouteProps) {
  return <Route {...rest} render={props => <Component {...props} />} />;
}

export default RoutePublic;
