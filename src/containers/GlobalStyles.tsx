import React from 'react';
import { createGlobalStyle } from 'styled-components';

import { appColor } from 'modules/theme';

const GlobalStyle = createGlobalStyle`
  *,
  *:before,
  *:after {
    box-sizing: border-box;
  }
  @font-face {
    font-family: 'font7d8dfef7a3d4102859703c8b19d32dba';

    src:    url('https://nomail.com.ua/files/eot/7d8dfef7a3d4102859703c8b19d32dba.eot?#iefix') format('embedded-opentype'),
    url('https://nomail.com.ua/files/woff/7d8dfef7a3d4102859703c8b19d32dba.woff') format('woff'),
    url('https://nomail.com.ua/files/woff2/7d8dfef7a3d4102859703c8b19d32dba.woff2') format('woff2');
  }

  @font-face { font-family: 'Museo Sans Cyrl';  src: local('MuseoSansCyrl-300'), url('https://nomail.com.ua/files/woff/7d8dfef7a3d4102859703c8b19d32dba.woff') format('woff'); }


  html {
    font-size: 62.5%;
    -webkit-font-smoothing: antialiased;
    height: 100%;
  }

  body {
    font-family: 'Museo Sans Cyrl';
    font-size: 16px; /* stylelint-disable unit-disallowed-list */
    margin: 0;
    min-height: 100vh;
    padding: 0;
  }

  img {
    height: auto;
    max-width: 100%;
  }

  a {
    color: ${appColor};
    text-decoration: none;

    &.disabled {
      pointer-events: none;
    }
  }

  button {
    appearance: none;
    background-color: transparent;
    border: 0;
    cursor: pointer;
    display: inline-block;
    font-family: inherit;
    line-height: 1;
    padding: 0;
  }
`;

export default function GlobalStyles() {
  return <GlobalStyle />;
}
